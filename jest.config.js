const { defaults } = require('jest-config');

module.exports = {
  preset: 'ts-jest',
  testEnvironment: 'node',
  setupFiles: [
    '<rootDir>/test.config/test-shim.js',
    '<rootDir>/test.config/test-setup.js'
  ],
  transform: {
    '^.+\\.[t|j]sx?$': 'babel-jest',
    '\\.ts$': '<rootDir>/test.config/jest-transforme.js'
  },
  transformIgnorePatterns: [
    '<rootDir>/node_modules/'
  ],
  moduleFileExtensions: [...defaults.moduleFileExtensions, 'ts', 'tsx']
};
