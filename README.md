# Starter React with Typescript
## 2020

## Usage

### Development

To run project firstly, install all dependecies

````
yarn
````
then
````
yarn dev
````
For types checking run in watch mode
````
yarn ts-check:watch
````
### Tests
To run tests in watch mode
````
yarn test:watch
````

To run tests one time
````
yarn test
````

## Core

- [Typescript](https://www.typescriptlang.org/)

## Tools configured

- [TSLint](https://palantir.github.io/tslint/) - maintain javascript coding standards.
- [Emotion](https://emotion.sh/docs/introduction) - writing css styles with JavaScript.

## Libraries used

- [React](https://reactjs.org/)
- [React Router](https://reacttraining.com/react-router/)
- [Mobx](https://mobx.js.org/)
- [Webpack](https://webpack.js.org/) - application bundler
