import { observable, action } from 'mobx';

export default class SpinnertState {

  @observable isPageShow = false;

  @action showPage(): void {
    this.isPageShow = true;
  }

  @action hidePage(): void {
    setTimeout(() => {
      this.isPageShow = false;
    }, 500);
  }
}
