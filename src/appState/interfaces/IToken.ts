export default interface IToken {
  accessToken: string;
  expiresAt: number;
  expiresIn: number;
  refreshExpiresAt: number;
  refreshExpiresIn: number;
  refreshToken: string;
  tokenType: string;
}
