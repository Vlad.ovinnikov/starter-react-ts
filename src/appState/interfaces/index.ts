import Token from './IToken';
import User from './IUser';
import Post from './IPost';

export type IToken = Token;
export type IUser = User;
export type IPost = Post;
