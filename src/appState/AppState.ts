import * as React from 'react';
import { History } from 'history';

import SpinnerState from './SpinnerState';
import { history } from '../index';
import Axios, { UserApi, PostApi } from './apis';
import { AuthService } from '../services';
import { UserStateService, PostStateService } from './states';

export class AppState {
  readonly AuthService: AuthService;
  readonly spinner: SpinnerState;
  readonly history: History;
  readonly users: UserStateService;
  readonly posts: PostStateService;

  constructor(historyState: History) {

    this.AuthService = new AuthService();

    const axios = new Axios(this.AuthService);
    const userApi = new UserApi(axios);
    const postApi = new PostApi(axios);

    this.history = historyState;
    this.spinner = new SpinnerState();
    this.users = new UserStateService(userApi);
    this.posts = new PostStateService(postApi);
  }

  static createForContext(): AppState {
    return new AppState(history);
  }
}

const AppContext = React.createContext(AppState.createForContext());

export const Provider = AppContext.Provider;

export const useAppStateContext = (): AppState => {
  return React.useContext(AppContext);
};
