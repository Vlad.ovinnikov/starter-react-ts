import Token from './Token';
import { IToken } from '../../interfaces';

export default abstract class TokenFactory {
  abstract build(token: IToken): Token;
}
