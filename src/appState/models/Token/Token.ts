import { IToken } from '../../interfaces';
import { DataFormat } from '../../../utils';

export default class Token extends DataFormat implements IToken {
  accessToken: string;
  expiresAt: number;
  expiresIn: number;
  refreshExpiresAt: number;
  refreshExpiresIn: number;
  refreshToken: string;
  tokenType: string;

  constructor(token: IToken) {
    super();

    const {
      accessToken, expiresAt, expiresIn, refreshExpiresAt, refreshExpiresIn, refreshToken, tokenType
    }: IToken = token;

    this.tokenType = tokenType;
    this.accessToken = accessToken;
    this.refreshToken = refreshToken;
    this.expiresAt = expiresAt;
    this.expiresIn = expiresIn;
    this.refreshExpiresAt = refreshExpiresAt;
    this.refreshExpiresIn = refreshExpiresIn;
  }
}
