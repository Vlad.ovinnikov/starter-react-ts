import Token from './Token';
import TokenFactory from './TokenFactory';
import { IToken } from '../../interfaces';

export default class TokenBuilder extends TokenFactory {
  build(token: IToken): Token {
    return new Token(token);
  }
}
