import { IPost } from '../../interfaces';

export default class Post implements IPost {
  id: number;
  userId: number;
  body: string;
  title: string;

  constructor(post: IPost) {
    const {
      id, userId, body, title
    }: IPost = post;

    this.id = id;
    this.userId = userId;
    this.body = body;
    this.title = title;
  }
}
