import Post from './Post';
import Factory from './PostFactory';
import { IPost } from '../../interfaces';

export default class Builder extends Factory {
  buildMany(posts: IPost[]): Post[] {
    return posts.map((post: IPost): Post => {
      return new Post(post);
    });
  }
}
