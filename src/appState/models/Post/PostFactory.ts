import Post from './Post';
import { IPost } from '../../interfaces';

export default abstract class Factory {
  abstract buildMany(users: IPost[]): Post[];
}
