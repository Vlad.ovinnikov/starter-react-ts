import { IUser } from '../../interfaces';

export default class User implements IUser {
  id: number;
  username: string;
  name: string;
  email: string;

  constructor(user: IUser) {
    const {
      id, username, name, email
    }: IUser = user;

    this.id = id;
    this.username = username;
    this.name = name;
    this.email = email;
  }
}
