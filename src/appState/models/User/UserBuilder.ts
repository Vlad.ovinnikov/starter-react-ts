import User from './User';
import Factory from './UserFactory';
import { IUser } from '../../interfaces';

export default class Builder extends Factory {
  build(user: IUser): User {
    return new User(user);
  }
  buildMany(users: IUser[]): User[] {
    return users.map((user: IUser): User => {
      return new User(user);
    });
  }
}
