import User from './User';
import { IUser } from '../../interfaces';

export default abstract class Factory {
  abstract build(user: IUser): User;
  abstract buildMany(users: IUser[]): User[];
}
