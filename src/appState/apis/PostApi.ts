import { AxiosResponse } from 'axios';

import Axios from './index';
import { IPost } from '../interfaces';

const BASE_URL = '/posts';

export default class {

  private readonly axios: Axios;

  constructor(axios: Axios) {
    this.axios = axios;
  }

  list(): Promise<AxiosResponse<IPost[]>> {
    return this.axios.apiJson().get(`${ BASE_URL }`);
  }

  listByUserId(userId: number): Promise<AxiosResponse<IPost[]>> {
    return this.axios.apiJson().get(`${ BASE_URL }?userId=${ userId }`);
  }

}
