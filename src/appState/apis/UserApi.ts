import { AxiosResponse } from 'axios';

import Axios from './index';
import { IUser } from '../interfaces';

const BASE_URL = '/users';

class UserApi {

  private readonly axios: Axios;

  constructor(axios: Axios) {
    this.axios = axios;
  }

  list(): Promise<AxiosResponse<IUser[]>> {
    return this.axios.apiJson().get(`${ BASE_URL }`);
  }

  get(id: number): Promise<AxiosResponse<IUser>> {
    return this.axios.apiJson().get(`${ BASE_URL }/${ id }`);
  }

}

export default UserApi;
