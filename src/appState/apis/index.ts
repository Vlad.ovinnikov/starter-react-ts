import axios, { AxiosInstance, AxiosRequestConfig, AxiosError, AxiosResponse } from 'axios';

import { API_URL } from '../../constants';
import { ImageType } from '../../enums';
import { AuthService } from '../../services';

export { default as UserApi } from './UserApi';
export { default as PostApi } from './PostApi';

interface IHeaders {
  'Content-Type': string;
  Authorization?: string;
}

const BASE_URL = `${ API_URL }`;
const WITH_AUTH = false;
export default class Axios {

  readonly authService: AuthService;
  private isAlreadyFetchingAccessToken = false;

  constructor(authService: AuthService) {
    this.authService = authService;
  }

  apiJson = (): AxiosInstance => {
    const headers: IHeaders = {
      'Content-Type': 'application/json'
    };

    if (this.authService.token) {
      headers.Authorization = this.authService.token;
    }

    const instance: AxiosInstance = axios.create({
      headers,
      baseURL: BASE_URL
    });

    if (WITH_AUTH) {
      this.requestInterceptor(instance);
      this.responseInterceptor(instance);
    }

    return instance;
  }

  apiForm = (): AxiosInstance => {
    const headers: IHeaders = {
      'Content-Type': 'multipart/form-data'
    };
    if (this.authService.token) {
      headers.Authorization = this.authService.token;
    }

    const instance: AxiosInstance = axios.create({
      headers,
      baseURL: BASE_URL
    });

    if (WITH_AUTH) {
      this.requestInterceptor(instance);
      this.responseInterceptor(instance);
    }

    return instance;
  }

  private readonly requestInterceptor = async (instance: AxiosInstance): Promise<void> => {
    instance.interceptors.request.use(async (config: AxiosRequestConfig): Promise<AxiosRequestConfig> => {
      // Do refresh token
      const newConfig = await this.refreshToken(config);
      return newConfig;

    }, (error: AxiosError): Promise<AxiosError> => {
      return Promise.reject(error);
    });
  }

  private readonly responseInterceptor = (instance: AxiosInstance): number => {
    return instance.interceptors.response.use((response: AxiosResponse): Promise<AxiosResponse> => {

      if ([ImageType.JPG, ImageType.JPEG, ImageType.PNG].includes(response.headers['content-type'])) {
        response.data = window.URL.createObjectURL(new Blob([response.data]));
      }

      return Promise.resolve(response);

    },
      (error: AxiosError): Promise<AxiosError> => {
        if (error.response && [401, 403].includes(error.response.status) && window.location.pathname !== '/signin') {
          this.authService.logout();
        }

        if (error.message === 'Network Error') {
          window.location.replace('/unavailable');
          return Promise.reject('Service unavailable');
        }

        return Promise.reject(error.response);
      });
  }

  private readonly refreshToken = async (config: AxiosRequestConfig): Promise<AxiosRequestConfig> => {

    if (config.url !== '/login' && this.authService.tokenIsExpired) {

      if (this.authService.refreshTokenIsExpired) {
        this.authService.logout();
        console.warn('Refresh token has expired');
        return Promise.reject('Your session has expired!');
      }

      if (!this.authService.refreshToken) {
        this.authService.logout();
        console.warn('There is no refreshToken in localStorage');
        return Promise.reject('Your session has expired!');
      }

      if (!this.isAlreadyFetchingAccessToken) {
        this.isAlreadyFetchingAccessToken = true;

        const response = await axios({
          headers: { 'Content-Type': 'application/json' },
          method: 'post',
          url: `${ API_URL }/admin/refresh_token`,
          data: {
            refreshToken: this.authService.refreshToken
          }
        });

        if (!response.data) {
          this.authService.logout();
          console.error('Empty data on refresh token');
          return Promise.reject('Something wrong with response from server');
        }

        await this.authService.setToken(response.data);
        config.headers.Authorization = this.authService.token;
        this.isAlreadyFetchingAccessToken = false;
      }
    }

    return config;
  }
}
