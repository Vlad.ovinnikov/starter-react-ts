import { observable, action } from 'mobx';

import { PostApi } from '../apis';
import { Post, PostBuilder } from '../models';

export default class {

  @observable.ref posts: Post[] = [] as Post[];

  private readonly postApi: PostApi;

  constructor(postApi: PostApi) {
    this.postApi = postApi;
  }

  @action getList = async (): Promise<void> => {
    try {
      const response = await this.postApi.list();
      const builder = new PostBuilder();

      this.posts = builder.buildMany(response.data);

    } catch (error) {
      return error;
    }
  }

  @action getListByUserId = async (userId: number): Promise<void> => {
    try {
      const response = await this.postApi.listByUserId(userId);
      const builder = new PostBuilder();

      this.posts = builder.buildMany(response.data);

    } catch (error) {
      return error;
    }
  }

}
