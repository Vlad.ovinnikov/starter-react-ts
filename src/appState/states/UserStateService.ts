import { observable, action } from 'mobx';

import { UserApi } from '../apis';
import { User, UserBuilder } from '../models';

export default class {

  @observable.ref user: User | null = null;
  @observable.ref users: User[] | null = null;

  private readonly userApi: UserApi;

  constructor(userApi: UserApi) {
    this.userApi = userApi;
  }

  @action getList = async (): Promise<void> => {
    try {
      const response = await this.userApi.list();
      const builder = new UserBuilder();

      this.users = builder.buildMany(response.data);

    } catch (error) {
      return error;
    }
  }

  @action get = async (id: number): Promise<void> => {
    try {
      const response = await this.userApi.get(id);
      const builder = new UserBuilder();

      this.user = builder.build(response.data);

    } catch (error) {
      return error;
    }
  }

}
