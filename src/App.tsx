import * as React from 'react';
import * as H from 'history';
import { Redirect, Route, Switch, withRouter } from 'react-router-dom';
import { observer } from 'mobx-react';
import { Global } from '@emotion/react';

import { LayoutRoute, MainLayout, IntroLayout } from './components/layout';
import { AppState } from './appState/AppState';
import { globalStyles } from './styles';
import { Spinner } from './components/common';
import { withAppState } from './hocs';

const Intro = React.lazy(() => import(/* webpackChunkName: "Intro" */'./components/pages/Intro/Intro'));
/*eslint-disable */
const Posts = React.lazy(() => import(/*webpackChunkName: "Posts" */ './components/pages/Posts/Posts')
  .then((module: any) => module));
/*eslint-disable */
const Users = React.lazy(() => import(/* webpackChunkName: "Users" */ './components/pages/Users/Users')
  .then((module: any) => module));
/*eslint-disable */
const UserPosts = React.lazy(() => import(/* webpackChunkName: "UserPosts" */ './components/pages/UserPosts/UserPosts')
  .then((module: any) => module));

interface IMatchParams {
  name: string;
  id: string;
}

export interface IRouteComponentProps<P> {
  match: IMatch<P>;
  location: H.Location;
  history: H.History;
  /*eslint-disable */
  staticContext?: any;
  exact?: boolean;
}
export interface IMatch<P> {
  params: P;
  isExact: boolean;
  path: string;
  url: string;
}

export interface IAppProps extends IRouteComponentProps<IMatchParams> {
  appState: AppState;
}

@observer
class App extends React.Component<IAppProps> {

  render() {
    const { isPageShow } = this.props.appState.spinner;

    return (
      <React.Fragment>
        <Global styles={ globalStyles } />
        <Spinner loading={ isPageShow } />
        <React.Suspense fallback={ <Spinner loading={ true } /> }>
          <Switch>
            <IntroLayout
              path="/"
              component={ Intro }
              exact />
            <LayoutRoute
              path="/posts"
              layout={ MainLayout }
              component={ Posts }
              exact />
            <LayoutRoute
              path="/users"
              layout={ MainLayout }
              component={ Users }
              exact />
            <LayoutRoute
              path="/users/:id/posts"
              layout={ MainLayout }
              component={ UserPosts }
              exact />
            <Route
              path="**"
              render={ () => <Redirect to="/" /> }
              exact />
          </Switch>
        </React.Suspense>
      </React.Fragment>
    );
  }
}

export default withAppState(withRouter(App));
