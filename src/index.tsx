import * as React from 'react';
import ReactDOM from 'react-dom';
import { createBrowserHistory as createHistory } from 'history';
import { BrowserRouter } from 'react-router-dom';
import { setLogEnabled } from 'mobx-react-devtools';

import App from './App';
import { AppState, Provider } from './appState/AppState';

setLogEnabled(false);

export const history = createHistory();
export const appState = new AppState(history);

ReactDOM.render((
  <Provider value={ appState }>
    <BrowserRouter>
      <App />
    </BrowserRouter>
  </Provider>
), document.getElementById('root'));
