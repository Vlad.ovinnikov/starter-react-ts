export default class {
  static SaveObject<T>(name: string, obj: T): void {
    const serializedObj = JSON.stringify(obj);
    localStorage.setItem(name, serializedObj);
  }

  static GetObject<T>(key: string): T {
    const value = localStorage.getItem(key);
    return value && JSON.parse(value);
  }
}
