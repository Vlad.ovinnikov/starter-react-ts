import { AUTHORIZATION } from '../constants';
import LocalStorageService from './LocalStorageService';
import { DataFormat } from '../utils';
import { IToken } from '../appState/interfaces';
import { Token, TokenBuilder } from '../appState/models';

export interface IAuthService {
  logout(): void;
}

export default class AuthService extends DataFormat implements IAuthService {

  constructor() {
    super();
  }

  get tokenExpiresTime(): number {
    const tokenObj: IToken = LocalStorageService.GetObject(AUTHORIZATION);
    return tokenObj ? tokenObj.expiresAt : 0;
  }

  get refreshTokenExpiresTime(): number {
    const tokenObj: IToken = LocalStorageService.GetObject(AUTHORIZATION);
    return tokenObj ? tokenObj.refreshExpiresAt : 0;
  }

  get tokenIsExpired(): boolean {
    return this.tokenExpiresTime < this.currentTimestamp;
  }

  get token(): string {
    const tokenObj: IToken = LocalStorageService.GetObject(AUTHORIZATION);
    return tokenObj && `${ tokenObj.tokenType } ${ tokenObj.accessToken }`;
  }

  get refreshToken(): string {
    const tokenObj: IToken = LocalStorageService.GetObject(AUTHORIZATION);
    return tokenObj && tokenObj.refreshToken;
  }

  get refreshTokenIsExpired(): boolean {
    return this.refreshTokenExpiresTime < this.currentTimestamp;
  }

  setToken = async (tokenData: IToken): Promise<Token> => {
    const builder = new TokenBuilder();
    const token = builder.build(tokenData);

    // save the newly refreshed token for other requests to use
    await LocalStorageService.SaveObject(AUTHORIZATION, token);
    return token;
  }

  logout = (): void => {
    localStorage.removeItem(AUTHORIZATION);
    window.location.replace('/signin');
  }

}
