import * as React from 'react';
import { useAppStateContext } from '../appState/AppState';

export default (WrappedComponent: React.ElementType): React.ElementType => {
  /*eslint-disable */
  return (props: any) => {
    const appState = useAppStateContext();

    return <WrappedComponent appState={ appState } { ...props } />;
  };
};
