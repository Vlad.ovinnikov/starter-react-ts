import moment from 'moment-timezone';

export default class {

  get currentTimestamp(): number {
    return moment().unix();
  }

}
