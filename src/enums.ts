export enum Breakpoints {
  XS = 'xs',
  SM = 'sm',
  MD = 'md',
  LG = 'lg',
  XL = 'xl'
}

export enum MessageType {
  ERROR = 'ERROR',
  SUCCESS = 'SUCCESS'
}

export enum ImageType {
  JPG = 'image/jpg',
  JPEG = 'image/jpeg',
  PNG = 'image/png',
}
