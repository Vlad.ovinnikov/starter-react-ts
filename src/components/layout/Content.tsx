import * as React from 'react';
import styled from '@emotion/styled';

const Content = styled.div`
  padding: 15px;
  padding-bottom: 70px;
  background-color: lightgray;
`;

interface IProps {
  children: React.ElementType;
}

export default (props: IProps): JSX.Element => {
  const { ...restProps } = props;

  return <Content { ...restProps } >
    { restProps.children }
  </Content>;
};
