import * as React from 'react';
import { withSize } from 'react-sizeme';

import { Route } from 'react-router-dom';
import { Breakpoints } from '../../enums';

interface IProps {
  path: string;
  component: React.ElementType;
  layout: React.ElementType;
  exact?: boolean;
  size: {
    width: number | null;
    height: number | null;
  };
}

interface ISizeMeProps {
  size: {
    width: number | null;
    height: number | null;
  };
}

const LayoutRoute = (layoutProps: IProps) => {
  const { component: Component, layout: Layout, ...rest } = layoutProps;
  const query = ({ size }: ISizeMeProps) => {
    const { width } = size;
    if (!width) {
      return { breakpoint: Breakpoints.XS };
    }

    if (width < 575) {
      return { breakpoint: Breakpoints.XS };
    }

    if (width > 576 && width < 767) {
      return { breakpoint: Breakpoints.SM };
    }

    if (width > 768 && width < 991) {
      return { breakpoint: Breakpoints.MD };
    }

    if (width > 992 && width < 1199) {
      return { breakpoint: Breakpoints.LG };
    }

    if (width > 1200) {
      return { breakpoint: Breakpoints.XL };
    }

    return { breakpoint: Breakpoints.XS };
  };

  return (
    <Route { ...rest } render={ props => (
        <Layout { ...layoutProps } { ...props } >
          <Component { ...props } { ...query(layoutProps)} />
        </Layout>
    ) } />
  );
};

export default withSize()(LayoutRoute);
