import * as React from 'react';

import { Content, Footer, Header } from './index';
import { Breakpoints } from '../../enums';

interface IProps {
  breakpoint: Breakpoints;
  children: React.ElementType;
}

export default (props: IProps): JSX.Element => {
  return (
    <main>
      <Header/>
      <Content>{ props.children }</Content>
      <Footer />
    </main>
  );
};
