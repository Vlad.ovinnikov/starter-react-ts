import * as React from 'react';
import styled from '@emotion/styled';

const Footer = styled.footer`
  position: fixed;
  left: 0;
  right: 0;
  bottom: 0;
  color: darkgray;
  height: 35px;
  background-color: #000;
  padding: 5px 10px;
`;

export default (): JSX.Element => {
  return (
    <Footer>Footer</Footer>
  );
};
