import * as React from 'react';
import { Route } from 'react-router-dom';

interface IProps {
  component: React.ElementType;
  path: string;
  exact?: boolean;
}

export default (compProps: IProps): JSX.Element => {
  const { component: Component, ...rest } = compProps;

  return (
    <Route render={ props => <Component { ...props } { ...rest } /> } />
  );
};
