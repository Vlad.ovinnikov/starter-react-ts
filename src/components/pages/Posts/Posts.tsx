import * as React from 'react';
import { observer } from 'mobx-react';
import styled from '@emotion/styled';
import { Link } from 'react-router-dom';

import { Post } from '../../../appState/models';
import { IAppProps } from '../../../App';
import { withAppState } from '../../../hocs';

const Item = styled.div`
  margin-top: 5px;
  background-color: #fff;
  padding: 10px;
  margin-top: 25px;
  border: 1px solid darkgray;
  border-radius: 10px;
`;
const Title = styled.h3`

`;
const Text = styled.p`

`;
@observer
class Posts extends React.PureComponent<IAppProps> {

  componentDidMount() {
    this.loadData();
  }

  renderList = () => {
    const { posts } = this.props.appState.posts;

    if (!posts || posts.length === 0) {
      return null;
    }

    return posts.map((post: Post) => {
      const { id, title, body } = post;

      return <Item key={ id }>
        <Title>{ title }</Title>
        <Text>{ body }</Text>
      </Item>;
    });
  }
  render() {
    return (
      <React.Fragment>
        <Link to={ '/' }>Intro</Link>
        <h3>Posts</h3>
        { this.renderList() }
     </React.Fragment>
    );
  }

  private readonly loadData = async () => {
    this.props.appState.spinner.showPage();
    await this.props.appState.posts.getList();
    this.props.appState.spinner.hidePage();
  }
}

export default withAppState(Posts);
