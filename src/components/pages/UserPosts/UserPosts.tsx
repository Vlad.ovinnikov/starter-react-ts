import * as React from 'react';
import { observer } from 'mobx-react';
import styled from '@emotion/styled';
import { Link } from 'react-router-dom';

import { Post } from '../../../appState/models';
import { IAppProps } from '../../../App';
import { withAppState } from '../../../hocs';

const Item = styled.div`
  margin-top: 5px;
  background-color: #fff;
  padding: 10px;
  margin-top: 25px;
  border: 1px solid darkgray;
  border-radius: 10px;
`;
const Title = styled.h3`

`;
const Text = styled.p`

`;
@observer
class UserPosts extends React.PureComponent<IAppProps> {

  componentDidMount() {
    this.loadData();
  }

  renderList = () => {
    const { posts } = this.props.appState.posts;

    if (!posts || posts.length === 0) {
      return null;
    }

    return posts.map((post: Post) => {
      const { id, title, body } = post;

      return <Item key={ id }>
        <Title>{ title }</Title>
        <Text>{ body }</Text>
      </Item>;
    });
  }
  render() {
    const { user } = this.props.appState.users;
    return (
      <React.Fragment>
        <Link to={ '/users' }>Back to the users list</Link>
        <h3>{ user?.username }&apos;s Posts</h3>
        { this.renderList() }
     </React.Fragment>
    );
  }

  private readonly loadData = async () => {
    const { params } = this.props.match,
      id = parseInt(params.id, 10);

    this.props.appState.spinner.showPage();
    await this.props.appState.users.get(id);
    await this.props.appState.posts.getListByUserId(id);
    this.props.appState.spinner.hidePage();
  }
}

export default withAppState(UserPosts);
