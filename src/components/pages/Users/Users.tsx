import * as React from 'react';
import { observer } from 'mobx-react';
import styled from '@emotion/styled';
import { Link } from 'react-router-dom';

import { User } from '../../../appState/models';
import { IAppProps } from '../../../App';
import { withAppState } from '../../../hocs';

const Item = styled.div`
  margin-top: 5px;
`;

@observer
class Users extends React.PureComponent<IAppProps> {

  componentDidMount() {
    this.loadData();
  }

  renderList = () => {
    const { users } = this.props.appState.users;

    if (!users || users.length === 0) {
      return null;
    }

    return users.map((user: User) => {
      return <Item key={ user.id }>
        { user.name }: { user.email }
        <Link style={ { marginLeft: '10px' } }
          to={ `/users/${ user.id }/posts` }>Posts</Link>
      </Item>;
    });
  }
  render() {
    return (
      <React.Fragment>
        <Link to={ '/' }>Intro</Link>
        <h3>Users</h3>
        { this.renderList() }
     </React.Fragment>
    );
  }

  private readonly loadData = async () => {
    this.props.appState.spinner.showPage();
    await this.props.appState.users.getList();
    this.props.appState.spinner.hidePage();
  }
}

export default withAppState(Users);
