import * as React from 'react';
import styled from '@emotion/styled';
import { Link } from 'react-router-dom';

const Container = styled.div`
  position: absolute;
  top: 0;
  right: 0;
  bottom: 0;
  left: 0;
  padding: 20px;
  text-align: center;
  background-color: lightgray;

  & a {
    display: inline-block;
    font-weight: 600;
    width: 100%;
    margin-top: 10px;
  }
`;

export default (): JSX.Element => {
  return (
    <Container>
      <h2>Intro</h2>
      <p>There is starter for React projects with Typescript!</p>
      <h3>Pages:</h3>
      <Link to="/users">Users</Link>
      <Link to="/posts">Posts</Link>
    </Container>
  );
};
