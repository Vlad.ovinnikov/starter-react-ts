import * as React from 'react';
import styled from '@emotion/styled';
import { PulseLoader } from 'react-spinners';

interface IProps {
  loading: boolean;
  size: number;
  color: string;
  backdropColor: string;
  style?: React.CSSProperties;
}

const Spinner = (props: IProps): JSX.Element | null => {
  const { style, loading, color, size, backdropColor } = props;

  if (!loading) {
    return null;
  }

  const Wrapper = styled('div')`
      display: flex;
      position: absolute;
      top: 0;
      bottom: 0;
      left: 0;
      right: 0;
      align-items: center;
      justify-content: center;
      z-index: 9998;
    `,
    BounceLoaderWrapperStyled = styled('div')`
      position: absolute;
      margin: 0 auto;
      z-index: 9998;
    `,
    SpinnerBackdrop = styled('div')`
      position: absolute;
      top: 0;
      right: 0;
      bottom: 0;
      left: 0;
      background-color: ${ backdropColor };
      opacity: 0.8;
    `;

  return (
    <Wrapper style={ style }>
      <BounceLoaderWrapperStyled>
        <PulseLoader
          size={ size }
          color={ color }
          loading={ loading } />
      </BounceLoaderWrapperStyled>
      <SpinnerBackdrop />
    </Wrapper>
  );
};

Spinner.defaultProps = {
  backdropColor: 'lightgrey',
  color: 'lightgreen',
  size: 15
};

export default Spinner;
